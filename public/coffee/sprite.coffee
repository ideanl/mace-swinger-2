class g.Sprite
  x: 0
  y: 0

  imagePath: null

  constructor: (imagePath = null) ->
    if imagePath
      @image = new Image
      @image.src = imagePath
      @image.onload = => @ready = true
      { @width, @height } = @image

  draw: (context) ->
    context.drawImage(@image, @x, @y, @image.width, @image.height)

  remove: ->
    i = game.sprites.indexOf(this)
    game.sprites.slice(i, 1)

class g.Background extends g.Sprite
  constructor: (imagePath) ->
    super
    @image.width = $('canvas').get(0).width
    @image.height = $('canvas').get(0).height
    { @width, @height} = @image

#class g.Button extends g.Sprite
#  width: 0
#  height: 0
#  textColor: '#000'
#
#  constructor: (x, y, text, bgColor, width = 0, height = 0, textColor = '#000') ->
#    @text = text
#    @color = bgColor
#    @width = width
#    @height = height
#    @textColor = textColor
#    @x = x
#    @y = y
#
#  draw: (context) ->
#    context.fillStyle = @color
#    context.rect(@x, @y, @width, @height)
#    context.fill()
#
#    context.fillStyle = @textColor
#    context.font = "32px Arial"
#    context.fillText @text, @x + @width / 2 - context.measureText(@text).width / 2, @y + @height / 2 + 8
