#-----------GAME------------
window.g = {}

FPS = 60
STATES = ['loading', 'menu', 'playing', 'paused']
class g.Game

  state: null

  sprites: []

  constructor: ->
    @createCanvas()
    @prepareCanvas()
    @createSprites()
    @start()

  createCanvas: ->
    @state = 'loading'
    @canvas = document.createElement('canvas')
    $('body').append(@canvas)

  prepareCanvas: ->
    @canvas.width = window.innerWidth * 0.9
    @canvas.height = window.innerHeight * 0.9
    $(@canvas).css({'border':'3px solid black','position':'absolute','left':'50%', 'top':'50%', 'margin-left': "-#{@canvas.width / 2}px", 'margin-top': "-#{@canvas.height / 2}px"})
    @context = @canvas.getContext('2d')
    @state = 'menu'
    @sprites[@state] = []

  start: ->
     @timer = setInterval =>
       @run()
     , 1000 / FPS

  run: ->
    @sprites[@state].forEach (sprite) => sprite.draw(@context)
    if @state == 'menu'
      @loadMenu()

  createSprites: ->
    #BACKGROUNDS
    for m in STATES
      if m == 'menu'
        imagePath = "/images/menu-back.jpg"
    @sprites[@state].push(@background = new g.Background(imagePath))
    #@sprites['menu'].push(@playButton = new g.Button(@canvas.width / 2 - 160, @canvas.height / 2 - 25, "Play Game", "#666", 320, 50))
    $('body').append("<button id='startButton' class='btn btn-lg menu' style=''>Play Game</div>")

  loadMenu: ->
    @context.fillStyle = "#777"
    @context.font = "48px Arial"
    text = "Mace Swinger 2: Return of the Playdoh"
    @context.fillText text, (@canvas.width / 2) - (@context.measureText(text).width / 2), (@canvas.height * 0.2 / 2)
    $('.menu').show()

  playing: ->
    @state == 'playing'

  paused: ->
    @state == 'paused'
